#!/bin/sh

Dockerfiles=$(find . -type f -name "Dockerfile")
ret=0

echo -e "Dockerfiles found:\n$Dockerfiles\n"

for Dockerfile in $Dockerfiles
do
	image_name=$DOCKER_HUB_USERNAME/$(basename $(dirname $Dockerfile))
	echo "docker build -f $Dockerfile -t $image_name ."
	echo "docker build -f $Dockerfile -t $image_name ." >> build_log
	docker build -f $Dockerfile -t $image_name . >> build_log
	if [ $? -ne 0 ]
	then
		ret=1
		echo "Command failed"
	else
		echo "docker push $image_name"
		docker push $image_name
		if [ $? -ne 0 ]
		then
			ret=1
		fi
	fi
	echo -e "Build finished\n" >> build_log
done

exit $ret