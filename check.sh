#!/bin/sh

Dockerfiles=$(find . -type f -name "Dockerfile")   
ret=0

echo -e "Dockerfiles found:\n$Dockerfiles\n"

for Dockerfile in $Dockerfiles
do
	echo "hadolint $Dockerfile"
	hadolint $Dockerfile
	if [ $? -ne 0 ]
	then
		ret=1
		echo "Command failed"
	fi
done

exit $ret